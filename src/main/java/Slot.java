import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public abstract class Slot {
    public static DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");

    private String id = "";
    private String numberPlate = "";
    private int slot = 0;
    private LocalDateTime in;
    private LocalDateTime out = null;
    private int fee = 0;

    public Slot(String id, String numberPlate, int slot) {
        this.id = id;
        this.numberPlate = numberPlate;
        this.slot = slot;
        in = LocalDateTime.now();
    }

    public void setOut(LocalDateTime out) {
        this.out = out;
    }

    public void setFee(int fee) {
        this.fee = fee;
    }

    public int getFee() {
        return fee;
    }

    public LocalDateTime getIn() {
        return in;
    }

    public LocalDateTime getOut() {
        return out;
    }

    public String getId() {
        return id;
    }

    public String getNumberPlate() {
        return numberPlate;
    }

    public int getSlot() {
        return slot;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("====NexSoft Parking App====\n");
        stringBuilder.append(String.format("> ID           : %s\n", id));
        stringBuilder.append(String.format("> Plate Number : %s\n", numberPlate));
        stringBuilder.append(String.format("> Enter        : %s\n", dateTimeFormatter.format(in)));
        if (out != null) {
            stringBuilder.append(String.format("> Exit         : %s\n", dateTimeFormatter.format(out)));
            stringBuilder.append(String.format("> Fee          : %d\n", fee));
            stringBuilder.append("> STATUS : OUT PARKING");
        } else {
            stringBuilder.append("> STATUS : IN PARKING");
        }
        stringBuilder.append("\n===========================\n");
        return stringBuilder.toString();
    }
}
