import java.util.Scanner;
import java.util.logging.Logger;

public class Main {
    private final Logger log = Log.getLogger();
    private ParkingLot parkingLot;

    public Main() {
        System.out.println("=============Welcome=============");
        System.out.println("=======NexSoft Parking App=======");
    }

    public void run(Scanner scanner) {
        int menuSelection;
        String selectionString;
        boolean isSelectionIsValid = false;

        do {
            showMenu();
            do {
                System.out.print("Input Choice [0-4] : ");
                selectionString = scanner.nextLine();
                isSelectionIsValid = selectionString.matches("[0-4]");
            } while (!isSelectionIsValid);
            menuSelection = Integer.parseInt(selectionString);

            switch (menuSelection) {
                case 1:
                    createParkingLot(scanner);
                    break;
                case 2:
                    if (parkingLot != null) {
                        addParkingVehicle(scanner);
                    } else {
                        System.out.println("[Please create a parking lot with menu selection '1']");
                    }
                    break;
                case 3:
                    if (parkingLot != null) {
                        removeParkingVehicle(scanner);
                    } else {
                        System.out.println("[Please create a parking lot with menu selection '1']");
                    }
                    break;
                case 4:
                    if (parkingLot != null) {
                        System.out.println(parkingLot);
                    } else {
                        System.out.println("[Please create a parking lot with menu selection '1']");
                    }
                    break;
                default:
                    menuSelection = 0;
                    break;
            }
        } while (menuSelection != 0);
    }

    private void removeParkingVehicle(Scanner scanner) {
        String plateNumber = "";
        boolean isPlateNumberIsValid = false;

        do {
            System.out.print("Input plate number [X-1234-XXX] : ");
            plateNumber = scanner.nextLine();
            isPlateNumberIsValid = checkPlateNumber(plateNumber);

            if (!isPlateNumberIsValid) {
                System.out.println("[Plate number not valid, please input valid number (X-1234-XXX)]");
            }
        } while (!isPlateNumberIsValid);

        Slot slot = parkingLot.unparkVehicle(plateNumber);
        if (slot != null) {
            System.out.println(slot);
            log.info(String.format("[OUT] Plate number %s from slot %d, total charges %d",
                    slot.getNumberPlate(), slot.getSlot() + 1, slot.getFee()));
        } else {
            System.out.printf("[Don't have vehicle with plate number : '%s'\n]", plateNumber);
            log.info(String.format("Plate number %s, not found", plateNumber));
        }
    }

    private void addParkingVehicle(Scanner scanner) {
        String plateNumber = "";
        boolean isPlateNumberIsValid = false;

        do {
            System.out.print("Input plate number [X-1234-XXX] : ");
            plateNumber = scanner.nextLine();
            isPlateNumberIsValid = checkPlateNumber(plateNumber);

            if (!isPlateNumberIsValid) {
                System.out.println("[Plate number not valid, please input valid number (X-1234-XXX)]");
            }
        } while (!isPlateNumberIsValid);

        Slot slot = parkingLot.parkVehicle(plateNumber);
        if (slot != null) {
            System.out.println(slot);
            log.info(String.format("[IN] Registered plate number %s", slot.getNumberPlate()));
        } else {
            System.out.println("[Sorry, the parking lot is full]");
            log.info(String.format("Can't register plate number %s, parking lot is full", plateNumber));
        }
    }

    private void createParkingLot(Scanner scanner) {
        String parkingLotSize = "";
        boolean isSizeValid = false;
        if (parkingLot == null) {
            do {
                System.out.print("Input Parking Lot slot size : ");
                parkingLotSize = scanner.nextLine();
                isSizeValid = checkParkingSize(parkingLotSize);

                if (!isSizeValid) {
                    System.out.println("[Please input valid size number]");
                }
            } while (!isSizeValid);
            parkingLot = new ParkingLot(Integer.parseInt(parkingLotSize));
        } else {
            System.out.println("[Parking Lot has been created]");
        }
    }

    private boolean checkPlateNumber(String plateNumber) {
        return plateNumber.matches("[A-Z]-\\d{4}-[A-Z]{3}");
    }

    private boolean checkParkingSize(String size) {
        return size.matches("([^0]\\d|[1-9])+\\d?");
    }

    private void showMenu() {
        System.out.println("===============Menu==============");
        System.out.println("1. Create Parking Lot");
        System.out.println("2. Park Vehicle");
        System.out.println("3. Unpark Vehicle");
        System.out.println("4. Parking Lot Status");
        System.out.println("0. Exit");
    }

    public static void main(String[] args) {
        System.setProperty("java.util.logging.SimpleFormatter.format", "%1$tY-%1$tm-%1$td %1$tH:%1$tM:%1$tS %5$s%6$s%n");
        Log.intializeLog();
        Main app = new Main();
        app.run(new Scanner(System.in));
    }
}
