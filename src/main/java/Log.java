import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class Log {
    private static final Logger logger = Logger.getLogger("ParkingLot");

    public static void intializeLog() {
        try {
            FileHandler fileHandler = new FileHandler("data.txt", false);
            SimpleFormatter simpleFormatter = new SimpleFormatter();
            logger.addHandler(fileHandler);
            fileHandler.setFormatter(simpleFormatter);

            logger.info("Logger Initialized, Program Started");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Logger getLogger() {
        return logger;
    }
}
