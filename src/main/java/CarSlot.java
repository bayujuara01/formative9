public class CarSlot extends Slot {
    private final String type = "Car";

    public CarSlot(String id, String numberPlate, int slot) {
        super(id, numberPlate, slot);
    }
}
