import java.util.Random;

public class SlotFactory {
    private static final String alphabets = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    public static Slot create(String type, String numberPlate, int slot) {
        if (type.toLowerCase().equals("car")) {
            return new CarSlot(generateId(), numberPlate, slot);
        } else {
            return null;
        }
    }

    private static String generateId() {
        Random random = new Random();
        char[] id = new char[12];
        for (int i = 0; i < id.length; i++) {
            id[i] =  alphabets.charAt(random.nextInt(alphabets.length() - 1));
        }
        return String.valueOf(id);
    }
}
