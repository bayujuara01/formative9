import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.params.provider.Arguments.arguments;

class ParkingLotTest {
    public ParkingLot parkingLot = new ParkingLot(10);

    @ParameterizedTest
    @MethodSource("calculateParkingFeeTestCasesFactory")
    void calculateParkingFee(LocalDateTime in, LocalDateTime out, int expected) {
        assertEquals(parkingLot.calculateParkingFee(in, out), expected);
    }

    static List<Arguments> calculateParkingFeeTestCasesFactory() {
        return List.of(
                arguments(
                        LocalDateTime.of(2021, 10, 7, 8,30),
                        LocalDateTime.of(2021, 10, 7, 9,30), 10000),
                arguments(
                        LocalDateTime.of(2021, 10, 7, 9,0),
                        LocalDateTime.of(2021, 10, 7, 12,0), 20000),
                arguments(
                        LocalDateTime.of(2021, 10, 7, 9,0),
                        LocalDateTime.of(2021, 10, 7, 12,30), 30000)
        );
    }


}