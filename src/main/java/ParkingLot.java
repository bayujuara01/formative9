import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.logging.Logger;

public class ParkingLot {
    private final Logger log = Log.getLogger();
    private final int defaultPrice = 10000;
    private Slot[] slots;

    /**
     * Parking lot constructor to create slot of parking lot, because initialize first size of array
     * @param numberOfslot accept Integer range for initialize arrays
     */
    public ParkingLot(int numberOfslot) {
        slots = new Slot[numberOfslot];
        log.info("Parking Lot created, with " + numberOfslot + " slot.");
    }

    /**
     * Add vehicle to parking lot, where return null if full
     * @param platNumber
     * @return if Slot null, that meaning parking lot is full
     */
    public Slot parkVehicle(String platNumber) {
        Slot slot = null;
        for (int i = 0; i < slots.length; i++) {
            if (slots[i] == null) {
                slot = SlotFactory.create("car", platNumber, i);
                slots[i] = slot;
                break;
            }
        }
        return slot;
    }

    /**
     * Remove Slot object from Array of Slot, where indicating that vehicle out from parking lot
     * In this method also calculate the fee.
     * @param platNumber
     * @return Slot - if Slot null, that meaning not found car with that platNumber
     */
    public Slot unparkVehicle(String platNumber) {
        Slot lastUnpark = null;
        for (int i = 0; i < slots.length; i++) {
            if (slots[i] != null && slots[i].getNumberPlate().equals(platNumber)) {
                lastUnpark = slots[i];
                lastUnpark.setOut(LocalDateTime.now());
                lastUnpark.setFee(calculateParkingFee(lastUnpark.getIn(), lastUnpark.getOut()));
                slots[i] = null;
                break;
            }
        }
        return lastUnpark;
    }

    /**
     * Calculating fee based on date and time where vehicle in and exit a parking lot
     * @param in date and time, when car going to parking lot
     * @param out date and time when car exit from parking lot
     * @return fee, where first 2 hours is 10.000, then each hour 10.000
     */
    public int calculateParkingFee(LocalDateTime in, LocalDateTime out) {
        Duration duration;
        int price = 0;
        if (out.isBefore(in.plusHours(2))) {
            price = defaultPrice;
        } else {
            out = out.minusHours(2);
            duration = Duration.between(in, out);
            price = (int) Math.ceil((double) duration.toMinutes() / 60.0) * defaultPrice;
            price += defaultPrice;
        }
        return price;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder("Slot No.  Registration No.\n");
        for (int i = 0; i < slots.length; i++) {
            if (slots[i] != null) {
                stringBuilder.append(String.format("%d.         %s\n", i + 1, slots[i].getNumberPlate()));
            } else {
                stringBuilder.append(String.format("%d.         Free\n", i + 1));
            }
        }
        return stringBuilder.toString();
    }
}
